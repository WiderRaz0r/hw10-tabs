const tabs = document.querySelector('.tabs');
tabs.addEventListener('click', (e) => {
    let currentActiveTarget = document.querySelector('.tabs .active');
    if (e.target !== e.currentTarget && e.target.classList.contains('tabs-title')) {
        currentActiveTarget.classList.remove('active');
        document.querySelector('.tabs-content .active-now').classList.remove('active-now');
        e.target.classList.add('active');
        document.querySelector(`[data-text="${e.target.innerText.toLowerCase()}"]`).classList.add('active-now');
    }
})